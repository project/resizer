Here's a step by step guide to getting the user picture resizer working in your drupal installation:

1. Unpack this module and upload to modules/resizer.

2. Download the Javascript Image Cropper from http://www.defusion.org.uk/code/javascript-image-cropper-ui-using-prototype-scriptaculous/

3. Create a 'cropper' directory within the resizer directory just created in your modules directory. (So you should now have modules/resizer/cropper)

4. Unpack the Javascript Image Cropper and upload the contents into the cropper directory you just created.

5. Enable the User Picture Resizer module in your Drupal admin panel.

6. Make sure User Pictures are enabled

7. Try uploading a user picture. All should be working :o)

