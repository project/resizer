			var cropStarted;
			function onEndCrop( coords, dimensions ) {
				$( 'edit-x1' ).value = coords.x1;
				$( 'edit-y1' ).value = coords.y1;
				$( 'edit-x2' ).value = coords.x2;
				$( 'edit-y2' ).value = coords.y2;
				$( 'edit-width' ).value = dimensions.width;
				$( 'edit-height' ).value = dimensions.height;
			}
			function startCropper(width, height) {
				if(!window.ourCropper) {
					window.ourCropper = 'true';
						
					// basic example
					Event.observe( 
						window, 
						'load',
						function() { 
								new Cropper.ImgWithPreview( 
									'testImage',
									{
										ratioDim: { x: width, y: height },
										minWidth: width, 
										minHeight: height,
										//maxWidth: width,
										//maxHeight: height,
				 						displayOnInit: true,
										onEndCrop: onEndCrop, 
										previewWrap: 'previewArea'
									}
								);
						}

					); 		
				}			
				
				if( typeof(dump) != 'function' ) {
					Debug.init(true, '/');
					
					function dump( msg ) {
						Debug.raise( msg );
					};
				} else dump( '---------------------------------------\\n' );
			}